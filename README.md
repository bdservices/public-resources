# Big Data Services Public Resources

## Big Data use cases
http://bigdatausecases.info/


## We got you
We bought courses for your soft landing.
Udemy user details:

**username**: Matarp13@gmail.com
**password**: Aa123456

Additional credentials are listed below.


## Airflow

An Airflow Fundamentals [course](https://academy.astronomer.io/astronomer-certification-apache-airflow-fundamentals-preparation) is available using the following credentials:

username : password

In addition, [Apache Airflow | A Real-Time & Hands-On Course on Airflow](https://www.udemy.com/course/apache-airflow/learn) is available for you to use using the following credentials:


Additional guides are can be found [here](https://www.astronomer.io/guides/) and documentation for operators can be found [here](https://registry.astronomer.io/modules/?types=operators&providers=Apache+Airflow+%2CSpark) and [here](https://airflow.apache.org/docs/apache-airflow/stable/_api/airflow/operators/index.html). 

## Spark
 
A free spark course is available [here](https://www.udacity.com/course/learn-spark-at-udacity--ud2002) (Lessons 1 - 3 and 5).

\* You must first sign-up or sign-in using Google or Facebook

A [quick start guide](https://spark.apache.org/docs/latest/quick-start.html), [configs](https://spark.apache.org/docs/latest/running-on-kubernetes.html) and more can be found on the [spark docs website](https://spark.apache.org/docs/latest/).

## Kafka
Want to start using Kafka?
[Learn Apache Kafka for Beginners v2](https://www.udemy.com/course/apache-kafka/learn/) for an easy start.

Intrested in Kafka Connect? 
start [Kafka Connect Hands-on Learning](https://www.udemy.com/course/kafka-connect/learn/)

## NiFi
For information about NiFI, we recommend taking [Apache NiFi - A Complete Guide (Hortonworks Data Platform)](https://www.udemy.com/course/apache-nifi-the-beginner-guide/learn)
